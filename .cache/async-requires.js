// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---src-templates-post-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/Post.jsx" /* webpackChunkName: "component---src-templates-post-jsx" */),
  "component---src-templates-resume-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/Resume.jsx" /* webpackChunkName: "component---src-templates-resume-jsx" */),
  "component---src-templates-portfolio-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/Portfolio.jsx" /* webpackChunkName: "component---src-templates-portfolio-jsx" */),
  "component---src-templates-portfolios-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/Portfolios.jsx" /* webpackChunkName: "component---src-templates-portfolios-jsx" */),
  "component---src-templates-list-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/List.jsx" /* webpackChunkName: "component---src-templates-list-jsx" */),
  "component---src-templates-tagged-list-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/TaggedList.jsx" /* webpackChunkName: "component---src-templates-tagged-list-jsx" */),
  "component---src-templates-categorized-list-jsx": () => import("/home/lupeluches94/supernucleoweb/src/templates/CategorizedList.jsx" /* webpackChunkName: "component---src-templates-categorized-list-jsx" */),
  "component---cache-dev-404-page-js": () => import("/home/lupeluches94/supernucleoweb/.cache/dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-404-jsx": () => import("/home/lupeluches94/supernucleoweb/src/pages/404.jsx" /* webpackChunkName: "component---src-pages-404-jsx" */),
  "component---src-pages-index-jsx": () => import("/home/lupeluches94/supernucleoweb/src/pages/index.jsx" /* webpackChunkName: "component---src-pages-index-jsx" */)
}

exports.data = () => import(/* webpackChunkName: "pages-manifest" */ "/home/lupeluches94/supernucleoweb/.cache/data.json")

